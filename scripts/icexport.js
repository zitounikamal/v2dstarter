
const fs = require('fs');
const path = require('path');
const exportPath = path.join(__dirname, "../iceCreamExport/gameplay.js");
const sourcePath = path.join(__dirname, "../build/static/js");



function clear(file) {
    return new Promise((resolve, reject) => {
        fs.writeFile(file, "", function (err) {
            if (err) {
                throw err;
                console.log(err);
            }
            resolve();
        });
    });
}


function appendFiles(source, dir) {
    return new Promise((resolve, reject) => {

        try {

            fs.readdir(dir, function (err, files) {
                //handling error
                if (err) {
                    return console.log('Unable to scan directory: ' + err);
                }
                let filesLoaded = 0;
                let maxFiles = files.length;
                //listing all files using forEach
                
                files.forEach(function (file) {
                    console.log('file > ',file);
                    if (file.indexOf(".txt") == -1) {
                        appendFile(source, dir + "/" + file).then(() => {
                            filesLoaded++;
                            if (filesLoaded >= maxFiles) {
                                resolve();
                            }
                        });
                    }else{
                        filesLoaded++;
                    }
                });
            });

        } catch (err) {
            console.log(err);
            reject();
        }
    });
}




function appendFile(file, appendFile) {
    return new Promise((resolve, reject) => {
        try {

            fs.readFile(appendFile, "utf8", (err, data) => {
                if (err) throw err;
                fs.appendFile(file, data, function (err) {
                    if (err) throw err;
                    console.log(appendFile + ' was appended');
                    resolve();
                });
            });


        } catch (err) {
            console.log(err);
            reject();
        }

    });

}



function startProcess() {
    clear(exportPath).then(() => {

        appendFiles(exportPath, sourcePath).then(() => {
            console.log("export ice success.");
        });


    });

}


startProcess();