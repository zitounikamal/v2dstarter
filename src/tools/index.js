import config from 'config';
import gsap from 'gsap/all';


class Tools {
    constructor() {
        console.log("Tools");

        this.DEG2RAD = 0.0174533;
        this.RAD2DEG = 57.2958;


    }

    async load(path, callback) {
        const data = await (await fetch(path)).json();
        callback(data);
    }



    angle2pos(centerPoint, rayon, angle) {
        var posX = centerPoint.x + (rayon * Math.cos((Math.PI) * (angle / 180)));
        var posY = centerPoint.y + (rayon * Math.sin((Math.PI) * (angle / 180)));
        return { x: posX, y: posY };
    }


    pos2angle(centerPoint, position) {
        return Math.atan2(centerPoint.y - position.y, centerPoint.x - position.x) * (180 / Math.PI);
    }


    toPoint(e, point = { x: 0, y: 0 }, offset = { x: 5, y: 5 }) {
        if (!e) {
            return point;
        }
        var source = e.changedTouches ? e.changedTouches[0] : (e.touches ? e.touches[0] : e);
        point.x = source.clientX - offset.x;
        point.y = source.clientY - offset.y + window.pageYOffset;
        return point;
    }

    distance(p0, p1) {
        return Math.sqrt((p1.x - p0.x) * (p1.x - p0.x) + (p1.y - p0.y) * (p1.y - p0.y));
    }



    getMovement(posRef, point) {

        var directionX;
        var directionY;

        posRef.x1 = posRef.x2;
        posRef.x2 = point.x;
        directionX = (point.x > posRef.x1) ? 1 : -1;

        posRef.y1 = posRef.y2;
        posRef.y2 = point.y;
        directionY = (point.y > posRef.y1) ? 1 : -1;


        return { x: directionX * this.distance({ x: posRef.x1, y: 0 }, { x: posRef.x2, y: 0 }), y: directionY * this.distance({ x: 0, y: posRef.y1 }, { x: 0, y: posRef.y2 }) };

    }







    /*
    Get Shape 2D size from points array //TODO size with rotation
    */

    getBoundingBox(shape = []) {

        var a = 9999, b = 0, c = 9999, d = 0;
        shape.map(pos => {
            a = Math.min(a, pos.x);
            b = Math.max(b, pos.x);
            c = Math.min(c, pos.y);
            d = Math.max(d, pos.y);
        });

        return { x: a, y: c, w: (b - a), h: (d - c) }
    }






    getMaxByProperty(arr, property) {

        let pr0 = property.split('.')[0];
        let pr1 = property.split('.')[1];
        let val;
        let max = 0;
        let obj;
        for (let i = arr.length - 1; i >= 0; i--) {
            val = pr1 ? arr[i][pr0][pr1] : arr[i][pr0];

            if (val >= max) {
                max = val;
                obj = arr[i];
            }
        }

        return obj;

    }


    match(regex, userAgent) {
        return (userAgent || '').search(regex) !== -1;
    }


    /**
     * how to use : setupIs().mobile 
     */

    setupIs(req, target = null) {
        const ua = req ? req.headers['user-agent'] : navigator.userAgent;

        // Setup is
        const is = target || {}

        // Browsers
        this.match(/Trident\/|MSIE/, ua) && (is.ie = true);
        this.match('Edge/', ua) && (is.edge = true);
        this.match('Firefox/', ua) && (is.firefox = true);
        this.match('Chrome/', ua) && (is.chrome = true);
        this.match('Safari', ua) && !this.match('Chrome', ua) && (is.safari = true);
        this.match(/(iPhone|iPad|iPod)/, ua) && (is.ios = true);
        this.match('Android', ua) && (is.android = true);

        // Devices
        (this.match(/iPad/i, ua) || (this.match(/Android/i, ua) && !this.match(/Mobile/i, ua))) && (is.tablet = true);
        ((is.ios || is.android) && !is.tablet) && (is.mobile = true);
        (!is.mobile && !is.tablet) && (is.desktop = true);

        // Images
        is.images = [
            (is.chrome || is.firefox) ? 'webm' : null,
            'png',
            'jpg',
        ].filter(item => item !== null);

        return is
    }






    hitTest(a, b) {
        return this.distance({ x: a.x, y: 0 }, { x: b.x, y: 0 }) < a.size.x / 2 + b.size.x / 2 &&
            this.distance({ x: 0, y: a.y }, { x: 0, y: b.y }) < a.size.z / 2 + b.size.z / 2 ? { x: a.x < b.x ? -1 : a.x > b.x ? 1 : 0, y: a.y < b.y ? 1 : a.y > b.y ? -1 : 0 } : false;
    }


    /**
     * check if a inside b throw XZ
     * @param {Object3D} a 
     * @param {Object3D} b 
     * @returns 
     */
    isInside(a, b) {
        return this.distance({ x: a.x, y: 0 }, { x: b.x, y: 0 }) + a.size.x / 2 < b.size.x / 2 &&
            this.distance({ x: 0, y: a.y }, { x: 0, y: b.y }) + a.size.z / 2 < b.size.z / 2;
    }









    //* DOM VERSION *//
    loadFont(fonts, callback) {

        this.fonts = fonts;
        this.callback = callback;

        this.strTest = "giItTWQy01234&@=-i?0";

        this._noFont = document.createElement("div");
        document.body.appendChild(this._noFont);
        this._noFont.innerText = this.strTest;
        this._noFont.style.display = "inline";
        this._noFont.style.visibility = "hidden";
        this._noFont.style.position = "fixed";

        this._myFont = document.createElement("div");
        document.body.appendChild(this._myFont);
        this._myFont.innerText = this.strTest;
        this._myFont.style.display = "inline";
        this._myFont.style.visibility = "hidden";
        this._myFont.style.position = "fixed";


        this.tryLoad();

        this.id = 0;

    };


    tryLoad() {

        this.timer = setInterval(() => {

            this._myFont.style.fontFamily = this.fonts[this.id];

            if (this._noFont.getBoundingClientRect().width !== this._myFont.getBoundingClientRect().width) {
                console.log(this.fonts[this.id] + " loaded");
                if (this.id >= this.fonts.length - 1) {
                    clearInterval(this.timer);
                    document.body.removeChild(this._noFont);
                    document.body.removeChild(this._myFont);
                    setTimeout(() => {
                        this.callback();
                    }, 100);
                } else {
                    this.id++;
                }
            }

            console.log("fonts loading");

        }, 100);
    }



    shuffle(array) {
        for (let i = array.length - 1; i > 0; i--) {
            const j = Math.floor(Math.random() * (i + 1));
            [array[i], array[j]] = [array[j], array[i]];
        }
    }



    range(min, max) {

        return min + (max - min) * Math.random();

    }







    /**
     get the ShortestAngle between 2 angles 
     from : radian angle
     to : radian angle
    retrun object from -> to
   */


    getShortestAngle(from, to) {

        let angle;
        let staticAngle = (((from * this.RAD2DEG) / 360 % 1) * 360);
        to = (((to * this.RAD2DEG) / 360 % 1) * 360);
        if (staticAngle - to <= 180) {
            if (Math.abs(staticAngle - to) > 180) {
                angle = -(360 - to);
            } else {
                angle = to;
            }
        } else {
            angle = 180 + (to + 180);
        }
        return { to: angle * this.DEG2RAD, from: staticAngle * this.DEG2RAD };

    }






    /**get pixel color
     * from context canvas
     * canvasContext : Context2D
     * x : int
     * y : int
     * return hexadecimal
     */

    getPixel(canvasContext, x, y) {
        var p = canvasContext.getImageData(x, y, 1, 1).data;
        var hex = "#" + ("000000" + this.rgbToHex(p[0], p[1], p[2])).slice(-6);
        return hex;
    }



    rgbToHex(r, g, b) {
        if (r > 255 || g > 255 || b > 255)
            throw "Invalid color component";
        return ((r << 16) | (g << 8) | b).toString(16);
    }



    hexToRgb(hex) {
        var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
        return result ? {
            r: parseInt(result[1], 16),
            g: parseInt(result[2], 16),
            b: parseInt(result[3], 16)
        } : null;
    }












    /**
    * 
    * @param {Array} a 
    */

    clearArray(a) {
        while (a.length > 0) {
            a.pop();
        }
    }








    cover(target, targetWidth, targetHeight) {

        target.scale.x = targetWidth;
        target.scale.y = target.scale.x;


        if (target.scale.y < targetHeight) {
            target.scale.y = targetHeight;
            target.scale.x = target.scale.y;
        }

    };




    numberFormat(nbr, digitsNumber) {
        var output = nbr + '';
        while (output.length < digitsNumber) {
            output = '0' + output;
        }
        return output;
    }





}

export default new Tools();