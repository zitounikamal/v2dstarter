
import Stats from 'lib/stats.module';
import { gsap } from 'gsap';
import config from 'config';
import Stage from './modules/levels/stage';
import assets from './assets';
import { Application, Container } from 'pixi.js';



export default class App extends Container {

  constructor() {

    super();

    this.init = this.init.bind(this);
    this.initEngine = this.initEngine.bind(this);
    this.update = this.update.bind(this);

    this.onResize = this.onResize.bind(this);
    this.initStage = this.initStage.bind(this);
    this.onLoadingProgress = this.onLoadingProgress.bind(this);
    this.debug = this.debug.bind(this);

    this.delta = 1;
    this.stats = null;
    this.stage = null;

  }






  init() {


    console.log("init::APP");

    document.backgroundColor = "#ffffff";

    document.body.style.margin = 0;
    document.body.style.display = "block";
    document.body.style["background-color"] = "#fff";
    document.body.style.color = "#fff";
    document.body.style.overflow = "hidden";

    this.view = document.createElement("canvas");
    this.view.style.width = "100%";
    this.view.style.height = "100%";
    this.view.style.position = "absolute";
    this.view.style.top = "0px";
    this.view.style.left = "0px";
    document.body.appendChild(this.view);

    this.initEngine();

    assets.on("onProgress", this.onLoadingProgress);
    assets.on("ready", this.initStage);
    assets.load();




  }




  onLoadingProgress(event) {

    console.log(parseInt(event.progress * 100) + "%");

  }






  initEngine() {


    console.log("APP::initEngine");


    this.app = new Application({
      view: this.view,
      width: 1,
      height: 1,
      backgroundColor: 0x220000,
      resolution: window.devicePixelRatio,
      autoDensity: true,
      antialias: true,
      resizeTo: window

    });



    this.mouse = this.app.renderer.plugins.interaction.mouse.global;
    this.stage = this.app.stage;


    //START ENGINE
    gsap.ticker.add(this.update);
    gsap.ticker.fps(config.fps);



  }





  initEvents() {

    console.log("APP::initEvents");

    window.addEventListener('resize', this.onResize, false);
    window.addEventListener("orientationchange", this.onResize, false);


  }










  onResize(size) {


    console.log("APP::onResize");

    let __width = size && size.width ? size.width : window.innerWidth;
    let __height = size && size.height ? size.height : window.innerHeight;


    this.level && this.level.onResize(__width, __height);

    //Force iOS view resize 
    setTimeout(() => {
      window.scrollTo(0, 1);
    }, 500);


  }







  initStage() {

    console.log("APP::initStage");

    this.level = new Stage();
    this.stage.addChild(this.level);


    this.initEvents();

    this.onResize();

    config.debug && this.debug();


  }






  debug() {

    //DEBUG

    this.stats = new Stats();
    document.body.appendChild(this.stats.dom);

  }




  //Destroy and reset current level

  dispose() {

    this.level && this.level.dispose();

  }










  update(a, b, c, time) {

    this.delta = b / (1 / config.fps) / 1000;

    this.level && this.level.update(this.delta);

    //Debug
    this.stats && this.stats.update();
    //


  }




}

