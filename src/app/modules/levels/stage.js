
import gsap from 'gsap/all';
import assets from 'app/assets';
import { Container, TextStyle, Text, DEG_TO_RAD } from 'pixi.js';



export default class Stage extends Container {

    constructor() {

        super();

        this.build();

        console.log("* My Level *");


    }



    build() {

        this.splash = assets.textures.splash;
        this.splash.tint = 0x00ff00;
        this.splash.anchor.set(.5);
        this.addChild(this.splash);

        this.splash.alpha = 0;

        gsap.to(this.splash.scale, { duration: .5, delay: .5, x: .7, y: .7, ease: "back.out(1.1)" });
        gsap.to(this.splash, { duration: .5, delay: .5, alpha: 1 });




        const style = new TextStyle({
            fontFamily: 'odinodin',
            fontSize: 36,
            fill: '#ffffff',
            wordWrap: true,
            wordWrapWidth: 500,
            lineHeight: 32
        });

        const richText = new Text('Welcome to V2DS (Voodooo 2D Starter)\nbased on WebGL / PixiJS for 2D games and interactive applications', style);
        richText.x = 20;
        richText.y = 100;

        this.addChild(richText);

    }



    onResize(w, h) {

        this.splash.x = w / 2;
        this.splash.y = h / 2;

    }



    dispose() {



    }


    update(delta) {

        this.splash.rotation += DEG_TO_RAD;

    }


}