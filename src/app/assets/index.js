
import config from "config";
import tools from "tools";
import { Howl } from "howler";
import { DisplayObject, Sprite } from "pixi.js";


class Assets extends DisplayObject {
    constructor() {

        super();

        this.ckeckInerval = 200; //ms
        this.totalItems = 3;  //json, audio, fonts
        this.loadedItems = 0;

        //Add texture's path here
        this.textures = {};
        this.textures.splash = config.assets + "/img/splash.png";

        // add textures to queue
        for (const key in this.textures) {
            this.totalItems++;
        }

    }





    load() {

        tools.load(config.assets + "/json/level.json", event => {

            console.log("json loaded");
            this.progressUpdate();

            this.fx = new Howl({
                src: [config.assets + "/audio/fx.mp3"],
                sprite: {
                    dong: [0, 235.1020408163265],
                    put: [2000, 208.97959183673453],
                    victory: [4000, 3578.775510204082],
                    fail: [9000, 288.52607709750623]
                },
                onload: () => {
                    console.log("Audio loaded");
                    this.progressUpdate();
                    // this.fx.play("dong");


                    tools.loadFont(["impact", "odinodin"], () => {
                        console.log("Fonts loaded");
                        this.progressUpdate();

                        for (const key in this.textures) {
                            let tmp = this.textures[key];
                            this.textures[key] = Sprite.from(tmp);
                        }


                        this.assetsLoaderInterval = setInterval(() => {

                            console.log("Textures loading...");

                            console.log(this.textures.splash.width)

                            var loaded = true;

                            for (const key in this.textures) {
                                if (Object.hasOwnProperty.call(this.textures, key)) {
                                    if (this.textures[key].width <= 1) {
                                        loaded = false;
                                    } else {
                                        !this.textures[key].loaded && this.progressUpdate();
                                        this.textures[key].loaded = true;
                                    }
                                } else {
                                    loaded = false;
                                }
                            }


                            if (loaded) {
                                console.log("Textures loaded");
                                this.assetsLoaderInterval && clearInterval(this.assetsLoaderInterval);
                            }


                        }, this.ckeckInerval);

                    });

                }
            });

        });

    }





    progressUpdate() {

        this.loadedItems++;
        this.progress = this.loadedItems / this.totalItems;
        this.emit("onProgress", { progress: this.progress });

        if (this.loadedItems >= this.totalItems) {
            this.emit("ready", { progress: this.progress });
        }

    }




}

export default new Assets();