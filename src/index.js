
import App from './app';
import config from 'config';

export default class Game {

  constructor() {

    let app = new App();
    config.app = app;


    app.init();



  }

}

//Launch
new Game();