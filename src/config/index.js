
class Config {
    constructor() {
        this.fps = 60;
        this.debug = true;
        // this.assets = process.env.PUBLIC_URL + "/assets";
        this.assets = "assets";
        this.app = null;
        this.stage = null;
        this.mouse = { x: 0, y: 0 };
    }
}

export default new Config();